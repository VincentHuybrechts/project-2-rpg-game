package be.dastudios.dilbeek.character.race;

import be.dastudios.dilbeek.character.Race;

import java.util.Map;

public class Dwarf extends Race {


    public Dwarf(String name, int hp, Map<String, Integer> attributes) {
        super(name, hp, attributes);
    }
}
