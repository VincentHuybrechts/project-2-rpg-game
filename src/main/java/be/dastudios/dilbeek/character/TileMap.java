package be.dastudios.dilbeek.character;

import java.io.Serializable;

public class TileMap implements Serializable {
    private String name;

    public TileMap(String name) {
        this.name = name;
    }

    public TileMap() {

    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
