package be.dastudios.dilbeek.character;

import java.util.HashMap;
import java.util.Map;

public class Monster extends Person  {

    public Monster(String name, int hp, Map<String, Integer> attributes) {
        super(name, hp, attributes);
    }

    public Monster() {

    }

    public static Monster createMonster(){

        Map<String, Integer> monsterAttributes =  new HashMap<>();
        monsterAttributes.put("Strength", 5);
        monsterAttributes.put("Intelligence", 5);
        monsterAttributes.put("Wisdom", 5);
        monsterAttributes.put("Dexterity", 5);
        monsterAttributes.put("Constitution", 5);
        monsterAttributes.put("Charisma", 5);
        Monster newMonster = new Monster("Goblin", 20, monsterAttributes);

        return newMonster;
    }


}
