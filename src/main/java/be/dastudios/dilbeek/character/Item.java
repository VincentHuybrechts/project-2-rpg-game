package be.dastudios.dilbeek.character;

public class Item {
    public Item(String name, int hp, int sp) {
        this.name = name;
        this.hp = hp;
        this.sp = sp;
    }




    private String name;
    private int hp;
    private int sp;

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public int getSp() {
        return sp;
    }
}
