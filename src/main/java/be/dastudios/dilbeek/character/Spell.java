package be.dastudios.dilbeek.character;

public class Spell {

    private String name;
    private int hp;
    private int sp;

    public Spell(String name, int hp, int sp) {
        this.name = name;
        this.hp = hp;
        this.sp = sp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getSp() {
        return sp;
    }

    public void setSp(int sp) {
        this.sp = sp;
    }
}
