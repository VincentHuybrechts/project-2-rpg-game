package be.dastudios.dilbeek.character;

import java.util.Map;

public abstract class Race  extends Person {

    public Race(String name, int hp, Map<String, Integer> attributes) {
        super(name, hp, attributes);
    }
}
