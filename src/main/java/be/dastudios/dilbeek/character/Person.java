package be.dastudios.dilbeek.character;

import java.io.Serializable;
import java.util.Map;

public abstract class Person implements Serializable {

    private String name;
    private int hp;
    private Map<String, Integer> attributes;
    private int initiative = 5;




    public Person(String name, int hp, Map<String, Integer> attributes) {
        this.name = name;
        this.hp = hp;
        this.attributes = attributes;
    }

    public Person() {

    }

    public Map<String, Integer> getAttributes() {
        return attributes;
    }

    public int getInitiative() {
        return initiative;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }
}
