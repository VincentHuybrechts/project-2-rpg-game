package be.dastudios.dilbeek.character.classs;

import be.dastudios.dilbeek.character.Gender;
import be.dastudios.dilbeek.character.Item;
import be.dastudios.dilbeek.character.Klasse;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Rogue extends Klasse implements Serializable {
    private int initiative = 6;

    public Rogue(String name, Gender gender, String race, String aClass, int hp, int stamina, Map<String, Integer> attributes) {
        super(name, gender, race, aClass, hp, stamina, attributes);
    }

    public List<Item> getInventory(){
        return inventory;
    }
    }

