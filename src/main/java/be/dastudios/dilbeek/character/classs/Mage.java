package be.dastudios.dilbeek.character.classs;

import be.dastudios.dilbeek.character.Gender;
import be.dastudios.dilbeek.character.Item;
import be.dastudios.dilbeek.character.Klasse;
import be.dastudios.dilbeek.character.Spell;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Mage extends Klasse implements Serializable {


    public Mage(String name, Gender gender, String race, String aClass, int hp, int stamina, Map<String, Integer> attributes) {
        super(name, gender, race, aClass, hp, stamina, attributes);
        List<Item> inventory = this.inventory;
    }


 public List<Item> getInventory(){
        return inventory;
 }

@Override
    public List<Spell> getSpells(){
        Spell lbolt = new Spell("Lightning bolt", 5, 3);
        Spell frbolt = new Spell("Frost breath", 3, 1);
        Spell fireBolt =  new Spell("Fire strike", 6, 2);
        List<Spell> mageSpells = new ArrayList<>();
        mageSpells.add(lbolt);
        mageSpells.add(frbolt);
        mageSpells.add(fireBolt);

        return mageSpells;
    }

}
