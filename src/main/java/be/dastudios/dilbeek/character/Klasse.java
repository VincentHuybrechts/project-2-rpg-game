package be.dastudios.dilbeek.character;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Klasse extends Person implements Serializable {

    private Gender gender;
    private String race;
    private String aClass;
    private int sp;
    public static List<Item> inventory;
    private List<Spell> spells = new ArrayList<>();

    public Klasse() {

    }

    public Klasse(String name, Gender gender, String race, String aClass, int hp, int stamina, Map<String, Integer> attributes) {
        super(name, hp, attributes);
        this.gender = gender;
        this.race = race;
        this.aClass = aClass;
        this.sp = stamina;

    }

    public List<Spell> getSpells() {
        return spells;
    }

    public Gender getGender() {
        return gender;
    }

    public String getRace() {
        return race;
    }

    public String getaClass() {
        return aClass;
    }

    public static void addItem(Item item){
        if (inventory.size() >= 10) {
            System.out.println("Your inventory is full");

        } else  {
            inventory.add(item);
        }
    }

    public  List<Item> getStartingInventory() {
        Item healingPotion = new Item("Healing Potion", 20, 0);
        Item staminaPotion = new Item("Stamina Potion", 0, 5);
        Item superPotion = new Item("Super Potion", 10, 10);
        List<Item> potions = new ArrayList<>();
        potions.add(healingPotion);
        potions.add(staminaPotion);
        potions.add(superPotion);
        return inventory;
    }


    public int getSp() {
        return sp;
    }

    public void setSp(int sp) {
        this.sp = sp;
    }
}

