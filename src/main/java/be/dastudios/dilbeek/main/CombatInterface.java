package be.dastudios.dilbeek.main;

import be.dastudios.dilbeek.character.Item;
import be.dastudios.dilbeek.character.Klasse;
import be.dastudios.dilbeek.character.Person;
import be.dastudios.dilbeek.character.Spell;
import be.dastudios.dilbeek.helper.KeyboardUtility;

import java.util.List;
import java.util.Locale;
import java.util.Random;

public interface CombatInterface {
    Random rng = new Random();

    static void combat(Klasse user, Person randomMonster) {

        if (user.getHp() <= 0) {
            System.out.printf("the %s killed you. Game over\n ", randomMonster.getName());
            GameInterface.menu();

        }
        if (randomMonster.getHp() <= 0) {
            System.out.printf("You killed the %s. Congratulations, you advance.\n ", randomMonster.getName());
            GameInterface.moveUser();

        }

        int userInitiative = rng.nextInt(21) + user.getInitiative() + user.getAttributes().get("Dexterity") / 2;
        int monsterInitiative = rng.nextInt(21) + (randomMonster.getAttributes().get("Dexterity")) / 2 + randomMonster.getInitiative();

        System.out.println("1. Basic Attack\n2. Skill/Spell\n3. Use Item\n4. Flee");
        System.out.println("Please enter your choice");
        String choiceUser = KeyboardUtility.askForText("> ");
        if (userInitiative >= monsterInitiative) {
           userAttack(user,randomMonster,choiceUser);
            System.out.println("monster hp: " + randomMonster.getHp());
           if(randomMonster.getHp()>0){
           monsterAttack(user,randomMonster);}
           else {combat(user,randomMonster);}

        } else { monsterAttack(user, randomMonster);
            System.out.println(user.getHp());
            if (user.getHp()>0){
            userAttack(user,randomMonster,choiceUser);
        }
                combat(user,randomMonster);

        }
        combat(user,randomMonster);
    }

    static void userAttack(Klasse user, Person randomMonster, String choiceUser){
    if (choiceUser.equals("1")) {
        basicAttack(user, randomMonster);

    } else if (choiceUser.equals("2")) {
        useSpell(user, randomMonster);
    } else if (choiceUser.equals("3")) {
        useItem(user, randomMonster);
    } else if (choiceUser.equals("4")) {
        flee(user, randomMonster);
    }
}

    static void monsterAttack(Klasse person, Person monster) {
        int damage = 0;
        damage = rng.nextInt(monster.getAttributes().get("Strength"));
        System.out.printf("%s attacks %s with basic attack and deals %d damage.\n", monster.getName(), person.getName(), damage);
        person.setHp(person.getHp() - damage);


    }

    static void basicAttack(Klasse person, Person monster) {
        Random rng = new Random();
        //int randomIntTo20 = rng.nextInt(21);

        int damage = rng.nextInt(person.getAttributes().get("Strength"));
        System.out.printf("%s attacks %s with basic attack and deals %d damage.\n", person.getName(), monster.getName(), damage);
        monster.setHp(monster.getHp() - damage);


    }

    private static void flee(Person user, Person randomMonster) {
        Random rng = new Random();
        int randomNumb = rng.nextInt(user.getAttributes().get("Dexterity"));
        int randomNumb2 = rng.nextInt(randomMonster.getAttributes().get("Dexterity"));
        if (randomNumb > randomNumb2) {
            System.out.println("Fleeing successful");
            GameInterface.moveUser();
        } else {
            System.out.println("Failed, combat goes on...");
        }

    }

    private static void useItem(Klasse user, Person randomMonster) {
        List<Item> inventory = user.getStartingInventory();
        Item chosenItem = chooseItem(inventory);
        if (chosenItem.getHp() > 0 || chosenItem.getSp() > 0) {
            user.setHp(user.getHp() + chosenItem.getHp());
            user.setSp(user.getSp() + chosenItem.getSp());
        } else if (chosenItem.getHp() < 0) {
            randomMonster.setHp(user.getHp() + chosenItem.getHp());

        }
        combat( user, randomMonster);

    }

    private static Spell chooseSpell(Klasse person, Person monster) {
        List<Spell> spells = person.getSpells();
        int choiceNumber = 0;
        String choiceSpell;
        if (person.getaClass().toLowerCase(Locale.ROOT).equals("mage")) {
            for (int i = 0; i < spells.size(); i++) {
                System.out.println((i + 1) + ". " + spells.get(i).getName());
            }
            choiceSpell = KeyboardUtility.askForText("Type the number to choose a spell");

        } else {
            for (int i = 0; i < spells.size(); i++) {
                System.out.println((i + 1) + ". use " + spells.get(i));
            }
            choiceSpell = KeyboardUtility.askForText("Type the number to choose a skill");

        }


        while (true) {
            if (choiceSpell.equals("1")) {
                choiceNumber = 0;
            } else if (choiceSpell.equals("2")) {
                choiceNumber = 1;
            } else if (choiceSpell.equals("3")) {
                choiceNumber = 2;
            } else if (choiceSpell.equals("4")) {
                choiceNumber = 3;
            } else if (choiceSpell.equals("5")) {
                choiceNumber = 4;
            } else if (choiceSpell.toLowerCase(Locale.ROOT).equals("return")) {
                combat(person, monster);
            } else {
                choiceNumber = spells.size();
            }
            if (choiceNumber >= spells.size()) {
                System.out.println("Choice doesn't exist. Try again: ");

                choiceSpell = KeyboardUtility.askForText("> ");
            } else {
                return spells.get(choiceNumber);
            }
        }


    }

    private static void useSpell(Klasse person, Person monster) {
        Spell spell = chooseSpell(person, monster);
        if (spell.getHp() < 0) {
            System.out.printf("using %s on self\n", spell.getName());
            person.setHp(person.getHp() + spell.getHp());
            person.setSp(person.getSp() + spell.getSp());
        } else {
            System.out.printf("using %s on %s \n", spell.getName(), monster.getName());
            monster.setHp(monster.getHp() - spell.getHp());
        }

    }

     static Item chooseItem(List<Item> inventory) {

         System.out.println("What item do you want to use?");
         for (int i = 0; i < inventory.size(); i++) {
             System.out.println(inventory.get(i));
         }

        String itemChoice = KeyboardUtility.askForText("> ");

int choiceNumber = 50;
        while (true) {
            if (itemChoice.equals("1")) {
                choiceNumber = 0;
                return inventory.get(choiceNumber);
            } else if (itemChoice.equals("2")) {
                choiceNumber = 1;
                return inventory.get(choiceNumber);
            } else if (itemChoice.equals("3")) {
                choiceNumber = 2;
                return inventory.get(choiceNumber);
            } else if (itemChoice.equals("4")) {
                choiceNumber = 3;
                return inventory.get(choiceNumber);
            } else if (itemChoice.equals("5")) {
                choiceNumber = 4;
                return inventory.get(choiceNumber);
            } else {
                System.out.println("Choice doesn't exist. Try again: ");

               itemChoice = KeyboardUtility.askForText("> ");


            }

            }


    }
}
