package be.dastudios.dilbeek.main;

import be.dastudios.dilbeek.character.Klasse;
import be.dastudios.dilbeek.character.TileMap;
import be.dastudios.dilbeek.helper.KeyboardUtility;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public interface FileInterface {


     static void saveCharacter(Klasse userCharacter, TileMap map){
        Path myDocuments = FileSystemView.getFileSystemView().getDefaultDirectory().toPath();
        Path savedDataFolder = myDocuments.resolve("Game save data");

        if(Files.exists(savedDataFolder)){
            //System.out.println("Game save data folder already exists!");
        } else {
            //System.out.print("Creating saved data folder...");
            try {
                Files.createDirectory(savedDataFolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //System.out.println("success!");
        }

        Path characterFile = savedDataFolder.resolve(userCharacter.getRace() + "-" + userCharacter.getaClass() +"-"+ userCharacter.getName() + ".cha");

        //System.out.printf("Saving user character object to %s file...", characterFile.getFileName());

        if(Files.exists(characterFile)){
            //System.out.println("File  already exists!");
        } else {
            //System.out.print("Creating file...");
            try {
                Files.createFile(characterFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //System.out.println("Character saved!");
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(characterFile.toString());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(userCharacter);
            objectOutputStream.writeObject(map);
            objectOutputStream.close();
            System.out.println("character saved!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadGame() {
        File userCharacterFile = showSavedFiles();
        System.out.printf("Loading character...");

        try {
            FileInputStream fileInputStream = new FileInputStream(userCharacterFile.toString());
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Klasse loadedUserCharacter = (Klasse) objectInputStream.readObject();
            TileMap loadedMap = (TileMap) objectInputStream.readObject();
            objectInputStream.close();
            System.out.println("success!");
            //System.out.println(loadedUserCharacter +"/"+ loadedMap);
            GameInterface.game(loadedUserCharacter,loadedMap);

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    public static File showSavedFiles() {

        Path myDocuments = FileSystemView.getFileSystemView().getDefaultDirectory().toPath();
        Path savedDataFolder = myDocuments.resolve("Game save data");
        try {
            List<File> files = Files.list(Paths.get(String.valueOf(savedDataFolder)))
                    .map(Path::toFile)
                    .filter(File::isFile)
                    .collect(Collectors.toList());

            for (int i = 0; i < files.size(); i++) {
                System.out.printf("%d %s\n", i+1, files.get(i).getName() );
            }
            String choiceUser = KeyboardUtility.askForText("\nWhich save file do you want to use?");
            int choiceNumber = 100;
            while (true) {
                if (choiceUser.equals("1")) {
                    choiceNumber = 0;
                } else if (choiceUser.equals("2")) {
                    choiceNumber = 1;
                } else if (choiceUser.equals("3")) {
                    choiceNumber = 2;
                } else if (choiceUser.equals("4")) {
                    choiceNumber = 3;
                } else if (choiceUser.equals("5")) {
                    choiceNumber = 4;
                } else if (choiceUser.toLowerCase(Locale.ROOT).equals("return")) {
                    ;
                } else {
                    choiceNumber = files.size();
                }
                if (choiceNumber >= files.size()) {
                    choiceUser = KeyboardUtility.askForText("\nChoice doesn't exist. Try again: \n");
                } else {
                    return files.get(choiceNumber);
                }
        } }
        catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    public static void resetSavedFiles(){
        Path myDocuments = FileSystemView.getFileSystemView().getDefaultDirectory().toPath();
        Path savedDataFolder = myDocuments.resolve("Game save data");
        File savedData = savedDataFolder.toFile();
        for(File file: savedData.listFiles())
            if (!file.isDirectory())
                file.delete();
    }
}

