package be.dastudios.dilbeek.main;

import be.dastudios.dilbeek.character.Gender;
import be.dastudios.dilbeek.character.Klasse;
import be.dastudios.dilbeek.character.Monster;
import be.dastudios.dilbeek.character.TileMap;
import be.dastudios.dilbeek.character.classs.Mage;
import be.dastudios.dilbeek.character.classs.Ranger;
import be.dastudios.dilbeek.character.classs.Rogue;
import be.dastudios.dilbeek.character.classs.Warrior;
import be.dastudios.dilbeek.helper.KeyboardUtility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static be.dastudios.dilbeek.main.CombatInterface.combat;


public interface GameInterface extends CombatInterface {

    static void menu(){
        System.out.println("*************************");
        System.out.println("Legend of the  lamb  III");
        System.out.println("*************************");
        System.out.println("D.A. Studios Dilbeek");
        System.out.println("*************************");

        System.out.println("What do you want to do?");
        System.out.println("New - Start a new game");
        System.out.println("Load - Load a  game");
        System.out.println("Reset - Reset all saved files");
        System.out.println("Controls - Game Controls");
        System.out.println("Quit - Quit game");

        String answerUSer = KeyboardUtility.askForText("\n>");


        if (answerUSer.toLowerCase(Locale.ROOT).equals("new")) {
            newGame();
        } else if (answerUSer.toLowerCase(Locale.ROOT).equals("load")) {
            FileInterface.loadGame();

        } else if (answerUSer.toLowerCase(Locale.ROOT).equals("reset")) {
            FileInterface.resetSavedFiles();
            menu();
        } else if (answerUSer.toLowerCase(Locale.ROOT).equals("controls")) {

        } else if (answerUSer.toLowerCase(Locale.ROOT).equals("quit")) {
            System.out.println("Quitting game...");
            System.exit(0);
        }

    }

    static void newGame() {
        System.out.println("Starting new game....");
        System.out.println("1. Fields of Generix");
        System.out.println("2. Cave of Thread");
        System.out.println("What map do you want to play?");
        int userMapChoice = KeyboardUtility.askForInt("> ");
        TileMap map1= new TileMap("Fields of Generix");
        TileMap map2= new TileMap("Cave of Thread");
        Klasse user = characterCreation();
        if (userMapChoice ==1){
            System.out.println("Map is being created...");
            System.out.println("Fields of Generix loaded.");
            FileInterface.saveCharacter(user, map1);

        } else if (userMapChoice==2){
            System.out.println("Map is being created...");
            FileInterface.saveCharacter(user, map2);
            System.out.println("Caves of Thread loaded.");
        }

        System.out.println("Your character is being created...");


        System.out.printf("%s the %s %s %s created.\n",user.getName(),user.getGender().toString().toLowerCase(Locale.ROOT),user.getRace(),user.getaClass());


        if(userMapChoice==1){
            System.out.println("Entering");
            game(user,map1);
        } else if (userMapChoice==2){
            game(user,map2);
        } else {
            System.out.println("Map doesn't exist.");
        }


    }
    static void game(Klasse user, TileMap map) {

        System.out.println("A wild goblin appears");
        combat(user,Monster.createMonster());
    }

    static void moveUser(){
        System.out.println("You moved away from action");
        GameInterface.menu();

    }

    static String chooseClass() {
        System.out.println("What is your class?");
        String klasse = KeyboardUtility.askForText("> ");
        return klasse;
    }

    static Ranger createRanger(String name, Gender gender, String race, Map<String, Integer> attributes) {
        Map<String, Integer> endAttributes = raceAttributes(race, attributes);
        endAttributes.replace("Dexterity", endAttributes.get("Dexterity") + 3);
        endAttributes.replace("Wisdom", endAttributes.get("Wisdom") + 2);


        int hp = endAttributes.get("Constitution") * 5;
        int sp = endAttributes.get("Dexterity") * 5;
        Ranger ranger = new Ranger(name, gender, race, "Ranger", hp, sp, endAttributes);
        return ranger;
    }

    static Mage createMage(String name, Gender gender, String race, Map<String, Integer> attributes) {
        Map<String, Integer> endAttributes = raceAttributes(race, attributes);
        endAttributes.replace("Wisdom", endAttributes.get("Wisdom") + 3);
        endAttributes.replace("Intelligence", endAttributes.get("Intelligence") + 2);

        int hp = endAttributes.get("Constitution") * 5;
        int sp = endAttributes.get("Wisdom") * 5;
        Mage mage = new Mage(name, gender, race, "Mage", hp, sp, endAttributes);
        return mage;
    }

    static Rogue createRogue(String name, Gender gender, String race, Map<String, Integer> attributes) {
        Map<String, Integer> endAttributes = raceAttributes(race, attributes);
        endAttributes.replace("Dexterity", endAttributes.get("Dexterity") + 3);
        endAttributes.replace("Intelligence", endAttributes.get("Intelligence") + 2);

        int hp = endAttributes.get("Constitution") * 5;
        int sp = endAttributes.get("Dexterity") * 5;
        Rogue rogue = new Rogue(name, gender, race, "Rogue", hp, sp, endAttributes);
        return rogue;
    }

    static Warrior createWarrior(String name, Gender gender, String race, Map<String, Integer> attributes) {
        Map<String, Integer> endAttributes = raceAttributes(race, attributes);
        endAttributes.replace("Strength", endAttributes.get("Strength") + 3);
        endAttributes.replace("Constitution", endAttributes.get("Constitution") + 2);

        int hp = endAttributes.get("Constitution") * 5;
        int sp = endAttributes.get("Dexterity") * 5;
        Warrior warrior = new Warrior(name, gender, race, "Warrior", hp, sp, endAttributes);
        return warrior;
    }

    static Map<String, Integer> raceAttributes(String race, Map<String, Integer> attributes) {
        if (race.equals("dwarf")) {
            attributes.replace("Strength", attributes.get("Strength") + 3);
            attributes.replace("Constitution", attributes.get("Constitution") + 3);

        } else if (race.equals("elf")) {
            attributes.replace("Dexterity", attributes.get("Dexterity") + 3);
            attributes.replace("Intelligence", attributes.get("Intelligence") + 3);
        } else if (race.equals("human")) {
            for (String key : attributes.keySet()) {
                attributes.replace(key, attributes.get(key) + 1);
            }
        }
        return attributes;
    }

    static Klasse characterCreation() {
        System.out.println("What is your adventurer's name?");
        String name = KeyboardUtility.askForText("> ");
        System.out.println("What is your gender?");
        Gender gender = Gender.valueOf(KeyboardUtility.askForText("> ").toUpperCase(Locale.ROOT));
        System.out.println("Can you tell me more about him, such as what race he is?");
        String race = KeyboardUtility.askForText("> ");
        Map<String, Integer> startingAttributes = new HashMap<>();
        startingAttributes.put("Strength", 10);
        startingAttributes.put("Intelligence", 10);
        startingAttributes.put("Wisdom", 10);
        startingAttributes.put("Dexterity", 10);
        startingAttributes.put("Constitution", 10);
        startingAttributes.put("Charisma", 10);
        if (!(race.toLowerCase(Locale.ROOT).equals("dwarf") || race.toLowerCase(Locale.ROOT).equals("elf") || race.toLowerCase(Locale.ROOT).equals("human"))) {
            System.out.println("Race doesn't exist. Try again");
            race = KeyboardUtility.askForText("> ");
        } else if (race.toLowerCase(Locale.ROOT).equals("dwarf")) {
            race = "dwarf";

        } else if (race.toLowerCase(Locale.ROOT).equals("elf")) {
            race = "elf";
        } else if (race.toLowerCase(Locale.ROOT).equals("human")) {
            race = "human";

        }
        String klasse = chooseClass().toLowerCase(Locale.ROOT);
        boolean a = true;
        while (true) {
            if (klasse.equals("ranger")) {
                return createRanger(name, gender, race, startingAttributes);

            } else if (klasse.equals("mage")) {
                return createMage(name, gender, race, startingAttributes);

            } else if (klasse.equals("warrior")) {
                return createWarrior(name, gender, race, startingAttributes);

            } else if (klasse.equals("rogue")) {
                return createRogue(name, gender, race, startingAttributes);

            } else {
                System.out.println("Class doesn't exist.");
                klasse = chooseClass().toLowerCase(Locale.ROOT);
            }
        }

    }
}

