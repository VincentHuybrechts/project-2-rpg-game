package be.dastudios.dilbeek.helper;


import java.util.Scanner;

public class KeyboardUtility {

    private static final Scanner INPUT = new Scanner(System.in);

    public static int askForInt(String message) {
        System.out.print( message);
        int userInput = INPUT.nextInt();
        INPUT.nextLine();
        return userInput;
    }

    public static double askForDouble(String message) {
        System.out.print( message);
        double userInput = INPUT.nextDouble();
        INPUT.nextLine();
        return userInput;
    }

    public static String askForText(String message) {
        System.out.print(message);
        return INPUT.next();
    }

    public static void shutdown() {
        INPUT.close();
    }

}
